// force start on top
$(window).on('beforeunload', function(){
    $(window).scrollTop(0);
});

$(document).ready(function() {
    // vimeo video
    var vimeo = $(".vimeo");
    var iframe = $(".vimeo-iframe");
    var player = new Vimeo.Player(iframe);

    $('.watch-video').on('click', function () {
        vimeo.show();
        $('body').css('overflowY', 'hidden');
    });

    $('.stop-video').on('click', function () {
        console.log('Ended the video!');
        vimeo.hide();
        player.unload();
        $('body').css('overflowY', 'scroll');
    });

    // jquery visible animations on load
    // intro
    var menuHome = $('#menuHome'),
        menuAbout = $('#menuAbout'),
        menuTeam = $('#menuTeam'),
        menuRoadmap = $('#menuRoadmap'),
        menuTestimonials = $('#menuTestimonials'),
        menuFaq = $('#menuFaq'),
        menuContact = $('#menuContact'),
        desktopMenu = $('.desktop-menu .inner ul li a');

    // desktop phone
    loadInViewFromLeft($('#intro-phone'),500);

    loadInViewUp($('#intro-text'),100);
    loadInViewUp($('#intro-button'),100);
    loadInViewUp($('#intro-button2'),100);

    loadInViewUp($('#intro-buttons'),900);


    // mobile phone
    setTimeout(function(){
        if ( $('#intro-phone2').visible(true) ) {
            $('#intro-phone2').addClass('fade-up');
        }
    }, 500);

    // force animation on scroll classes after 2 seconds
    setTimeout(function(){
        if ( !$('#intro-phone').hasClass('slide-from-left') || !$('#intro-phone2').hasClass('fade-up') || !$('#intro-buttons').hasClass('fade-in') || !$('#intro-text').hasClass('fade-in')) {
            $('#intro-text').addClass('fade-in');
            $('#intro-phone').addClass('slide-from-left');
            $('#intro-phone2').addClass('fade-up');
            $('#intro-buttons').addClass('fade-in');
        }
    }, 2000);

    // menu toggle
    $('.mobileNavIcon').on('click', function() {
        $(this).toggleClass('close');

        $('body').toggleClass('no-scroll');
        $('header .logo').toggleClass('transparent');

        // header is NOT sticky when menu is clicked
        if ( !$('header').hasClass('sticky-header') ) {
            $('header').toggleClass('sticky-header');
        }

        // if header IS sticky
        if ( $('.fullscreen-menu').hasClass('open') && $(window).scrollTop() < 10 ) {
            $('header').toggleClass('sticky-header');
        }

        $('.fullscreen-menu').fadeToggle('slow');
        $('.fullscreen-menu').toggleClass('open');
    });

    // menu toggle from menu
    $('.fullscreen-menu ul li a').on('click', function() {
        $('.mobileNavIcon').toggleClass('close');

        $('body').toggleClass('no-scroll');
        $('header .logo').toggleClass('transparent');

        $('header').toggleClass('sticky-header');
        $('.fullscreen-menu').fadeToggle('slow');
    });

    // menu smooth scroll
    $(".fullscreen-menu a").click(function(e){
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 60
        }, 1500);
        e.preventDefault(); //this is the important line.
    });

    // header background change
    $(window).scroll(function() {
        // console.log($(window).scrollTop());
        if ($(window).scrollTop() > 10) {
            $('header').addClass('sticky-header');
        } else {
            $('header').removeClass('sticky-header');
        }
    });

    // roadmap more text
    $(".roadmap .more").click(function(e){
        var _this = $(this),
            openRoadMap = $('.roadmap .more.open');


        // reset all
        if ( openRoadMap ) {
            openRoadMap.not(this).parent().find('.more-text').slideToggle('fast');
            openRoadMap.not(this).find('p').text('More');
            openRoadMap.not(this).removeClass('open');
        }

        // toggle state
        if ( _this.hasClass('open') ) {
            _this.removeClass('open');
            _this.find('p').text('More');

        } else {
            _this.addClass('open');
            _this.find('p').text('Hide');
        }

        _this.parent().find('.more-text').slideToggle('fast');

    });

    // jquery visible animations on scroll
    $(window).on("scroll", function () {
        //home
        if ( $('#home').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuHome.addClass('selected');
        }

        // wallet-items
        if ( $('#wallet').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuWallet.addClass('selected');
        }

        loadInViewUp($('#about-item-1'),0);
        loadInViewUp($('#about-item-2'),200);
        loadInViewUp($('#about-item-3'),400);
        loadInViewUp($('#about-item-4'),600);
        loadInViewUp($('#about-item-5'),800);

        // roadmap
        if ( $('#roadmap').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuRoadmap.addClass('selected');
        }

        loadInViewUp($('#road-item-road-1'),0);
        loadInViewUp($('#road-item-road-2'),200);
        loadInViewUp($('#road-item-road-3'),200);
        loadInViewUp($('#road-item-road-4'),200);
        loadInViewUp($('#road-item-road-5'),200);
        loadInViewUp($('#road-item-road-6'),200);
        loadInViewUp($('#road-item-road-7'),200);
        loadInViewUp($('#road-item-road-8'),200);
        loadInViewUp($('#road-item-road-9'),200);
        loadInViewUp($('#road-item-road-10'),200);

        loadInViewFromRight($('#road-item-text-1'),0);
        loadInViewFromRight($('#road-item-text-2'),200);
        loadInViewFromRight($('#road-item-text-3'),200);
        loadInViewFromRight($('#road-item-text-4'),200);
        loadInViewFromRight($('#road-item-text-5'),200);
        loadInViewFromRight($('#road-item-text-6'),200);
        loadInViewFromRight($('#road-item-text-7'),200);
        loadInViewFromRight($('#road-item-text-8'),200);
        loadInViewFromRight($('#road-item-text-9'),200);
        loadInViewFromRight($('#road-item-text-10'),200);

        // team members
        if ( $('#team').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuTeam.addClass('selected');
        }

        loadInViewUp($('#team-member1'),0);
        loadInViewUp($('#team-member2'),200);
        loadInViewUp($('#team-member3'),400);
        loadInViewUp($('#team-member4'),600);
        loadInViewUp($('#team-member5'),800);

        // FAQ
        if ( $('#faq').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuFaq.addClass('selected');
        }

        loadInViewUp($('#accordionExample'),0);


        // testimonials
        if ( $('#testimonials').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuTestimonials.addClass('selected');
        }

        loadInViewUp($('#testimonials-item-1'),0);
        loadInViewUp($('#testimonials-item-2'),200);
        loadInViewUp($('#testimonials-item-3'),400);
        loadInViewUp($('#testimonials-item-4'),600);

        // contact
        if ( $('#contact').visible(true) ) {
            desktopMenu.removeClass('selected');
            menuContact.addClass('selected');
        }

        loadInViewFromLeft($('#contact-info'),0);

    });

    // FUNCTIONS
    function loadInViewUp(target, delay) {
        if ( target.visible(true) ) {
            setTimeout(function(){
                target.addClass('fade-up');
            }, delay);
        }
    }

    function loadInViewFromLeft(target, delay) {
        if ( target.visible(true) ) {
            setTimeout(function(){
                target.addClass('slide-from-left');
            }, delay);
        }
    }

    function loadInViewFromRight(target, delay) {
        if ( target.visible(true) ) {
            setTimeout(function(){
                target.addClass('slide-from-right');
            }, delay);
        }
    }

});