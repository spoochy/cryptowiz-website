<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cryptowiz &#8211; Simple metrics, great insights!</title>

    <!-- Hotjar Tracking Code for https://cryptowiz.app -->
    <script>!function(t,h,e,j,s,n){t.hj=t.hj||function(){(t.hj.q=t.hj.q||[]).push(arguments)},t._hjSettings={hjid:1266755,hjsv:6},s=h.getElementsByTagName("head")[0],(n=h.createElement("script")).async=1,n.src="https://static.hotjar.com/c/hotjar-"+t._hjSettings.hjid+".js?sv="+t._hjSettings.hjsv,s.appendChild(n)}(window,document);</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137563110-1"></script>
    <script>function gtag(){dataLayer.push(arguments)}window.dataLayer=window.dataLayer||[],gtag("js",new Date),gtag("config","UA-137563110-1");</script>

    <link rel="shortcut icon" type="image/png" href="/assets/img/favicon.ico"/>

<!--    <link href="assets/css/style.css?v=--><?php //echo(date(" Y-m-d H:i:s ")); ?><!--" rel="stylesheet" type="text/css">-->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
    <header>
        <div class="container">
            <div class="inner">
                <div class="logo">
                    <div class="text">
                        <a href="#home" class="absolute"></a>
                        <h1>Crypto<span>wiz</span></h1>
                    </div>
                </div>
                <div class="menu">
                    <div class="mobileNavIcon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                    </div>
                    <div class="desktop-menu">
                        <div class="inner">
                            <ul>
                                <li>
                                    <a href="#home" id="menuHome" class="selected">Home</a>
                                </li>
                                <li>
                                    <a href="#about" id="menuAbout">Product</a>
                                </li>
                                <li>
                                    <a href="#team" id="menuTeam">Team</a>
                                </li>
                                <li>
                                    <a href="#roadmap" id="menuRoadmap">Roadmap</a>
                                </li>
                                <li>
                                    <a href="#testimonials" id="menuTestimonials">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#faq" id="menuFaq">FAQ</a>
                                </li>
                                <li>
                                    <a href="#contact" id="menuContact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="fullscreen-menu">
        <div class="container">
            <div class="inner">
                <ul>
                    <li>
                        <a href="#home">Home</a>
                    </li>
                    <li>
                        <a href="#about">Product</a>
                    </li>
                    <li>
                        <a href="#team">Team</a>
                    </li>
                    <li>
                        <a href="#roadmap">Roadmap</a>
                    </li>
                    <li>
                        <a href="#testimonials">Testimonials</a>
                    </li>
                    <li>
                        <a href="#faq">FAQ</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="intro" id="home">
        <div class="container">
            <div class="inner">
                <div class="left-side">
                    <div class="desktop-phone">
                        <div class="gif-container animated" id="intro-phone">
                            <div class="phoneAnimationGif">
                                <section class="topbar">
                                    <div class="inner">
                                        <div class="left">
                                            <p>10:41</p>
                                        </div>
                                        <div class="notch">
                                            <div class="bar"></div>
                                        </div>
                                        <div class="right">
                                            <div class="inner">
                                                <i class="fas fa-wifi"></i>
                                                <i class="fas fa-battery-full"></i>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="topbarContent">
                                    <div class="inner">
                                        <div class="title">
                                            <h2>Cryptowiz</h2>
                                        </div>
                                        <div class="menu">
                                            <div class="dots">
                                                <div class="dot"></div>
                                                <div class="dot"></div>
                                                <div class="dot"></div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="menuType">
                                    <div class="inner">
                                        <div class="type reports">
                                            <p>Reports</p>
                                        </div>
                                        <div class="type prices">
                                            <p>Prices</p>
                                        </div>
                                        <div class="type tools">
                                            <p>Tools</p>
                                        </div>
                                    </div>
                                </section>

                                <section class="highlightParent">
                                    <div class="inner">
                                        <div class="highlight"></div>
                                    </div>
                                </section>

                                <section class="menuList">
                                    <div class="inner">
                                        <div class="reportsType">
                                            <div class="items">
                                                <div class="item">
                                                    <p>bitcoin & altcoins</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>market cap</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>volume</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>payment coins</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>privacy coins</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>currency coins</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>ethereum</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pricesType">
                                            <div class="items">
                                                <div class="item">
                                                    <p>Global market</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>market prices</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="toolsType">
                                            <div class="items">
                                                <div class="item">
                                                    <p>rank calculator</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                                <div class="item">
                                                    <p>high comparison</p>
                                                    <i class="fas fa-info"></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right-side">
                    <div class="inner">
                        <div class="text animated" id="intro-text">
                            <p class="second-title">
                                CRYPTO ANALYSIS <br>AT YOUR FINGERTIPS.
                            </p>
                            <p>
                                Stay informed, spot trends  <br>
                                and make better decisions.
                            </p>
                        </div>

                        <div class="download-buttons animated" id="intro-buttons">
                            <div class="android">
                                <div class="img"></div>
                                <a href="https://play.google.com/store/apps/details?id=com.cryptowiz.app" class="absolute"></a>
                            </div>
                            <div class="ios">
                                <div class="img"></div>
                                <a href="https://itunes.apple.com/us/app/cryptowiz/id1452911504?ls=1&mt=8" class="absolute"></a>
                            </div>
                        </div>

                        <div class="buttons animated" id="intro-button">
                            <div class="watch-video">
                                <div class="inner">
                                    <i class="fas fa-play"></i>
                                    <p>Watch video</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mobile-phone">
<!--                    <div class="img animated mobile-phone" id="intro-phone2"></div>-->
                    <div class="gif-container animated" id="intro-phone2">
                        <div class="phoneAnimationGif">
                            <section class="topbar">
                                <div class="inner">
                                    <div class="left">
                                        <p>10:41</p>
                                    </div>
                                    <div class="notch">
                                        <div class="bar"></div>
                                    </div>
                                    <div class="right">
                                        <div class="inner">
                                            <i class="fas fa-wifi"></i>
                                            <i class="fas fa-battery-full"></i>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="topbarContent">
                                <div class="inner">
                                    <div class="title">
                                        <h2>Cryptowiz</h2>
                                    </div>
                                    <div class="menu">
                                        <div class="dots">
                                            <div class="dot"></div>
                                            <div class="dot"></div>
                                            <div class="dot"></div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="menuType">
                                <div class="inner">
                                    <div class="type reports">
                                        <p>Reports</p>
                                    </div>
                                    <div class="type prices">
                                        <p>Prices</p>
                                    </div>
                                    <div class="type tools">
                                        <p>Tools</p>
                                    </div>
                                </div>
                            </section>

                            <section class="highlightParent">
                                <div class="inner">
                                    <div class="highlight"></div>
                                </div>
                            </section>

                            <section class="menuList">
                                <div class="inner">
                                    <div class="reportsType">
                                        <div class="items">
                                            <div class="item">
                                                <p>bitcoin & altcoins</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>market cap</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>volume</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>payment coins</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>privacy coins</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>currency coins</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>ethereum</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pricesType">
                                        <div class="items">
                                            <div class="item">
                                                <p>Global market</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>market prices</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="toolsType">
                                        <div class="items">
                                            <div class="item">
                                                <p>rank calculator</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                            <div class="item">
                                                <p>high comparison</p>
                                                <i class="fas fa-info"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="vimeo">
        <div class="stop-video">
            <i class="fas fa-times"></i>
        </div>
<!--        <iframe class="vimeo-iframe" src="https://player.vimeo.com/video/309187025" frameborder="0" playsinline="false"></iframe>-->
        <iframe class="vimeo-iframe" src="https://player.vimeo.com/video/334038893" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

        <div class="overlay"></div>
    </div>

    <section class="about" id="about">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>Product</h2>
                    <p>About Cryptowiz</p>
                </div>

                <div class="grid-row">
                    <div class="item animated" id="about-item-1">
                        <div class="img"></div>
                        <h3 class="second-title">Benefits</h3>
                        <p>Cryptowiz helps you spot opportunities faster, stay up-to-date easier and make better informed decisions.</p>
                    </div>
                    <div class="item animated" id="about-item-2">
                        <div class="img"></div>
                        <h3 class="second-title">Users</h3>
                        <p>Investors, journalists and crypto-enthusiasts who want to follow the market regularly with minimum effort.</p>
                    </div>
                    <div class="item animated" id="about-item-3">
                        <div class="img"></div>
                        <h3 class="second-title">Why</h3>
                        <p>Cryptocurrency investors don't always have the skills or time to perform market analysis before making investments.</p>
                    </div>
                    <div class="item animated" id="about-item-4">
                        <div class="img"></div>
                        <h3 class="second-title">What</h3>
                        <p>Cryptowiz analyses complex financial data from the cryptocurrency market and converts it into short insightful reports.</p>
                    </div>
                    <div class="item filler"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="zigzag-divider" style="width: 100%;min-height: 14px;background: 0 repeat-x url('data:image/svg+xml;utf-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20width%3D%2214px%22%20height%3D%2212px%22%20viewBox%3D%220%200%2018%2015%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpolygon%20id%3D%22Combined-Shape%22%20fill%3D%22%23ebebeb%22%20points%3D%228.98762301%200%200%209.12771969%200%2014.519983%209%205.40479869%2018%2014.519983%2018%209.12771969%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E');"></div>

    <section class="team" id="team">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>Team</h2>
                    <p>Meet our team</p>
                    <div class="small-title-text">
                        <p>The transition to a decentralised economy is creating investment opportunities. We believe research, discipline and market analysis are required to capitalise.</p>
                        <ul>

                        </ul>
                    </div>
                </div>

                <div class="grid-row">
                    <div class="team-member Leedo animated" id="team-member1">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Leedo Daniel</p>
                                </div>
                                <div class="function">
                                    <p>CEO & Architect</p>
                                </div>
                                <div class="small-text">
                                    <p>FMR Senior Analyst at ABN AMRO Bank</p>
                                </div>
                            </div>
                            <div class="social">
                                <div class="linkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                    <a href="https://www.linkedin.com/in/leedo/" class="absolute"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member Gerard animated" id="team-member2">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Gerard van Beek</p>
                                </div>
                                <div class="function">
                                    <p>CTO & Lead Developer</p>
                                </div>
                                <div class="small-text">
                                    <p>FMR Global Program Manager eBusiness at ModusLink</p>
                                </div>
                            </div>
                            <div class="social">
                                <div class="linkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                    <a href="https://www.linkedin.com/in/gerardvanbeek/" class="absolute"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member Jordey animated" id="team-member3">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Jordey Knook</p>
                                </div>
                                <div class="function">
                                    <p>Front End Developer</p>
                                </div>
                                <div class="small-text">
                                    <p>Graphic Designer & Self proclaimed Space monkey</p>
                                </div>
                            </div>
                            <div class="social">
                                <div class="linkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                    <a href="https://www.linkedin.com/in/jordey-knook/" class="absolute"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member Stijn animated" id="team-member4">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Stijn Bakkeren</p>
                                </div>
                                <div class="function">
                                    <p>Board member</p>
                                </div>
                                <div class="small-text">
                                    <p>Finance, Tax & Accounting</p>
                                </div>
                            </div>
                            <div class="social">
                                <div class="linkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                    <a href="https://www.linkedin.com/in/stijn-bakkeren-6736538/" class="absolute"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member Bram animated" id="team-member5">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Bram Jonker</p>
                                </div>
                                <div class="function">
                                    <p>Board member</p>
                                </div>
                                <div class="small-text">
                                    <p>Strategy & Business Development</p>
                                </div>
                            </div>
                            <div class="social">
                                <div class="linkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                    <a href="https://www.linkedin.com/in/bramjonker/" class="absolute"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member filler animated"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="zigzag-divider" style="width: 100%;min-height: 14px;background: 0 repeat-x url('data:image/svg+xml;utf-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20width%3D%2214px%22%20height%3D%2212px%22%20viewBox%3D%220%200%2018%2015%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpolygon%20id%3D%22Combined-Shape%22%20fill%3D%22%23ebebeb%22%20points%3D%228.98762301%200%200%209.12771969%200%2014.519983%209%205.40479869%2018%2014.519983%2018%209.12771969%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E');"></div>

    <section class="roadmap" id="roadmap">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>Roadmap</h2>
                    <p>Key milestones</p>
                </div>

                <div class="road-item complete">
                    <div class="road animated" id="road-item-road-1">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-1">
                        <p>Q3 2018</p>
                        <p>Launch first prototype.</p>

                        <div class="more-text">
                            <p>We will release the first version of Cryptowiz as a web based app. Any person with a mobile web browser will be able to test it and share their feedback. This web app will be replaced with an IOS and Android version in the future. </p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item complete">
                    <div class="road animated" id="road-item-road-2">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-2">
                        <p>Q4 2018</p>
                        <p>Incorporate company and launch new website.  </p>

                        <div class="more-text">
                            <p>We will incorporate our company in The Netherlands. We will redesign our original website and add a short animation video explaining Cryptowiz. This new website will allow you to navigate more quickly and easily to find the information you want. </p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item complete">
                    <div class="road animated" id="road-item-road-3">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-3">
                        <p>Q1 2019</p>
                        <p>Launch beta version for IOS and Android.</p>

                        <div class="more-text">
                            <p>We will release four tools and four reports in our app. We will first go live with a limited number of tools & reports and release updates for the remainder on a regular basis. During this period we will also focus on improving the UI to ensure the best user experience. </p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item active">
                    <div class="road animated" id="road-item-road-4">
                        <div class="bullet pulse"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-4">
                        <p>Q2 2019</p>
                        <p>Update app with new features.</p>

                        <div class="more-text">
                            <p>We will add new functionality to existing tools and reports based on user feedback. A number of new tools and reports will also be added for a selected group of users to test. </p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item">
                    <div class="road animated" id="road-item-road-5">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-5">
                        <p>Q3 2019</p>
                        <p>Launch Pro version.</p>

                        <div class="more-text">
                            <p>Our Pro version will be a paid version. It will have all the features of the beta version plus several new updates for advanced users.</p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item">
                    <div class="road animated" id="road-item-road-6">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-6">
                        <p>Q4 2019</p>
                        <p>Add new languages to the app.</p>

                        <div class="more-text">
                            <p>We will analyse the geographic location of our users and determine if adding new languages to our app is a viable option. If needed we will add a number of new languages for users in specific regions.</p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item">
                    <div class="road animated" id="road-item-road-7">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-7">
                        <p>Q1 2020</p>
                        <p>Run major marketing campaign.</p>

                        <div class="more-text">
                            <p>We will launch a marketing campaign to promote our app on a large scale. This will include a set of new videos, the use of social media channels and a number of live events. </p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>
                <div class="road-item">
                    <div class="road animated" id="road-item-road-8">
                        <div class="bullet"></div>
                        <div class="line"></div>
                    </div>
                    <div class="text animated" id="road-item-text-8">
                        <p>Q2 2020</p>
                        <p>Launch Premium version.</p>

                        <div class="more-text">
                            <p>Our Premium version will be a paid version. It will have all the features of the Pro version plus several new updates for the most advanced users.</p>
                        </div>

                        <div class="btn more">
                            <p>More</p>
                        </div>
                    </div>
                </div>

<!--                <div class="road-item">-->
<!--                    <div class="road animated" id="road-item-road-10">-->
<!--                        <div class="bullet"></div>-->
<!--                    </div>-->
<!--                    <div class="text animated" id="road-item-text-10">-->
<!--                        <p>July</p>-->
<!--                        <p>Launch of Cryptowiz Premium. </p>-->
<!---->
<!--                        <div class="more-text">-->
<!--                            <p>Our Premium version will give users access to over 20 different tools and reports. This account will cost USD $1.50 per month, billed annually. We will continue adding new features to our Premium version based on user feedback.</p>-->
<!--                        </div>-->
<!---->
<!--                        <div class="btn more">-->
<!--                            <p>More</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </section>

    <div class="zigzag-divider" style="width: 100%;min-height: 14px;background: 0 repeat-x url('data:image/svg+xml;utf-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20width%3D%2214px%22%20height%3D%2212px%22%20viewBox%3D%220%200%2018%2015%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpolygon%20id%3D%22Combined-Shape%22%20fill%3D%22%23ebebeb%22%20points%3D%228.98762301%200%200%209.12771969%200%2014.519983%209%205.40479869%2018%2014.519983%2018%209.12771969%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E');"></div>

    <section class="testimonials team" id="testimonials">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>Testimonials</h2>
                    <p>Words from beta testers</p>
                </div>

                <div class="grid-row">
                    <div class="team-member testimonials-item animated" id="testimonials-item-1">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Tonny Chu</p>
                                </div>
                                <div class="small-text">
                                    <p>
                                        <span class="fas fa-quote-left"></span> The app is amazing!<br> I like the roadmap and the tools are easy to work with. I'm impressed. <span class="fas fa-quote-right"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member testimonials-item animated" id="testimonials-item-2">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Jasper Voorendonk</p>
                                </div>
                                <div class="small-text">
                                    <p>
                                        <span class="fas fa-quote-left"></span> This beta is quite handy. More functionality can be added in the future though. I'm sure this team will deliver! <span class="fas fa-quote-right"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member testimonials-item animated" id="testimonials-item-3">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Svetlana Katina</p>
                                </div>
                                <div class="small-text">
                                    <p>
                                        <span class="fas fa-quote-left"></span> I'm new to crypto and tried several other apps before. I love the design and simplicity of this app. It's a keeper. <span class="fas fa-quote-right"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team-member testimonials-item animated" id="testimonials-item-4">
                        <div class="inner">
                            <div class="img"></div>
                            <div class="text">
                                <div class="name">
                                    <p>Jerome Bryan</p>
                                </div>
                                <div class="small-text">
                                    <p>
                                        <span class="fas fa-quote-left"></span> I'm really excited about the upcoming monthly reports which will be added to the app soon. The app is really easy to use. <span class="fas fa-quote-right"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="zigzag-divider" style="width: 100%;min-height: 14px;background: 0 repeat-x url('data:image/svg+xml;utf-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20width%3D%2214px%22%20height%3D%2212px%22%20viewBox%3D%220%200%2018%2015%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpolygon%20id%3D%22Combined-Shape%22%20fill%3D%22%23ebebeb%22%20points%3D%228.98762301%200%200%209.12771969%200%2014.519983%209%205.40479869%2018%2014.519983%2018%209.12771969%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E');"></div>

    <section class="faq" id="faq">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>FAQ</h2>
                    <p>Get your answers</p>
                </div>

                <div class="accordion animated" id="accordionExample" style="transform: translateY(30px)">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    What is Cryptowiz?
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                Cryptowiz is a mobile app that converts large amounts of complex market data into short, useful and simple reports so any person can quickly understand what's happening in the cryptocurrency market. Cryptowiz also offer users a set of handy tools.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Is Cryptowiz free?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                Yes. The current beta version of our app is free and we will always have a free version available. We will also create a paid version for advanced users in the future.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How is our startup being funded? 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                 We still need to raise funds for our startup and plan to do so in the first half of 2019. The funds we raise will be use for marketing, further development and for adding new languages to Cryptowiz.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    How many tools & reports will be available?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                Our app will have 4 tools and 4 reports by the first half of 2019. We will double the amount of tools and reports by the end of 2019. Each tool and report will give you insights into a specific martket trend.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    What is the source for our data?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                We currently use data from coinmarketcap.com. In 2020 we will add new sources of data to our app to allow users to gain deeper market insights.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    Why is an invitation code required for the beta version?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                We prefer to limit the amount of beta testers early on. If you know someone who has a Cryptowiz account, you can get an invitation code from them. Once you create your own account you will automatically receive your own code to share with new users.  
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    How can I support this project?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                            <div class="card-body">
                                If you have any ideas of how we could make Cryptowiz better or how we can get the word out quickly when we launch, please contact me at daniel@cryptowizzy.com
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="zigzag-divider" style="width: 100%;min-height: 14px;background: 0 repeat-x url('data:image/svg+xml;utf-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20width%3D%2214px%22%20height%3D%2212px%22%20viewBox%3D%220%200%2018%2015%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpolygon%20id%3D%22Combined-Shape%22%20fill%3D%22%23ebebeb%22%20points%3D%228.98762301%200%200%209.12771969%200%2014.519983%209%205.40479869%2018%2014.519983%2018%209.12771969%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E');"></div>

    <section class="contact" id="contact">
        <div class="container">
            <div class="inner">
                <div class="title">
                    <h2>Contact</h2>
                    <p>Get in touch</p>
                </div>

                <div class="info animated" id="contact-info">
                    <div class="email">
                        <i class="fas fa-envelope"></i>
                        <div class="content">
                            <h4>Email</h4>
                            <p>contact@cryptowiz.app</p>
                        </div>
                    </div>
                    <div class="location">
                        <i class="fas fa-rocket"></i>
                        <div class="content">
                            <h4>Location</h4>
                            <p>The Netherlands</p>
                        </div>
                    </div>
                    <div class="twitter">
                        <a href="https://twitter.com/CryptowizUpdate" class="absolute"></a>
                        <i class="fab fa-twitter"></i>
                        <div class="content">
                            <h4>Follow</h4>
                            <p>twitter.com/cryptowizupdate</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="inner">
                <div class="img"></div>
                <p>Cryptowiz App BV - Chamber of Commerce number 72607874 - KDS 166, 3012GL, Rotterdam, The Netherlands - All Rights Reserved (c) 2019</p>
            </div>
        </div>
    </footer>

    <!--SCRIPTS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="assets/js/jquery.visible.min.js"></script>

    <!--vimeo player-->
    <script src="https://player.vimeo.com/api/player.js"></script>

    <!--main js-->
<!--    <script src="assets/js/main.min.js?v=--><?php //echo(date(" Y-m-d H:i:s ")); ?><!--"></script>-->
    <script src="assets/js/main.js"></script>
</body>
</html>
